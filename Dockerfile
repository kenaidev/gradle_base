# This file is a template, and might need editing before it works on your project.
FROM gradle:latest
WORKDIR /usr/src/myapp
COPY . /usr/src/myapp
RUN chmod +x ./gradlew
RUN pwd
RUN ls
RUN ./gradlew clean test --no-daemon
RUN ./gradlew build

